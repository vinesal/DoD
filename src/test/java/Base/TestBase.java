package Base;



import Pages.Homepage;
import Utilities.BrowserUtils;
import Utilities.WebDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


public class TestBase {
protected static WebDriver driver;
protected static Homepage hp;
    public TestBase() {
        PageFactory.initElements(driver, this);}
        @Before
        public void setupMethod () {

            driver = WebDriverFactory.getDriver("chrome");
            driver.manage().window().maximize();
            driver.get("https://dodgrantawards.dtic.mil/grants/index.html#/advancedSearch");
            driver.findElement(By.id("details-button")).click();
             // BrowserUtils.wait(2);
            driver.findElement(By.id("proceed-link")).click();
          //  BrowserUtils.wait();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            System.out.println("Page title is: "+ driver.getTitle());
        }

        @After
        public void tearDown () {
            System.out.println("closing");
            driver.quit();
        }

    }
