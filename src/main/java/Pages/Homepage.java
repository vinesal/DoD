package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class Homepage extends BasePage {

    public Homepage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "fiscalYear")
    public  WebElement year;
    @FindBy(id = "performSearch")
    public WebElement searchButton;


    public void search(String yearSelected) {
         System.out.println("searching");

        Select yearDrop = new Select(year);
        yearDrop.selectByVisibleText(yearSelected);

        System.out.println("Selected year: " + yearSelected);
        searchButton.click();

    }

        }


